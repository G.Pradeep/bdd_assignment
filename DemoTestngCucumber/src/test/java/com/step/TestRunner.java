package com.step;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(tags = "", features = "src/test/resources/Features/LoginTestng.feature", glue = "com.step",plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)
public class TestRunner extends AbstractTestNGCucumberTests{

}

