package com.step;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoCucumberUsingTestng {
	static WebDriver driver = null;

	@Given("User Must Launch The website")
	public void User_must_launch_the_website() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		System.out.println("browser launch");
	}
	@When("Click on the login profile button")
	public void click_on_the_login_profile_button(){
		driver.findElement(By.linkText("Log in")).click();
		
	}
	@When("user Enter the email in the Email field")
	public void user_enter_the_email_in_the_email_field() {
		driver.findElement(By.id("Email")).sendKeys("pradeepg123@gmail.com");
		
	}
	@When("user Ente the password in the Password field")
	public void user_ente_the_password_in_the_password_field() {
		driver.findElement(By.id("Password")).sendKeys("pradeepg");
	}
	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

	}
	@Then("the home page with logged usename should display")
	public void the_home_page_with_logged_usename_should_display() {
		System.out.println("logged user homepage is displayed");
	}
	@Then("click on the logout button")
	public void click_on_the_logout_button() {
		driver.findElement(By.linkText("Log out")).click();
	}

	@Then("user should taken to the homepage")
	public void user_should_taken_to_the_homepage() {
		System.out.println(" user should taken homepage is displayed");
	}

	@Then("close the browser")
	public void close_the_browser() {
		driver.quit();
	}
}
